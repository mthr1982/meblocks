//
//  UIActionSheet+Blocks.h
//  MEBlocks
//
//  Created by Mitsuharu Emoto on 2013/01/22.
//  Copyright (c) 2013年 Mitsuharu Emoto. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface UIActionSheet (Blocks) <UIActionSheetDelegate>

typedef void (^ActionSheetCompletion)(UIActionSheet *actionSheet, NSInteger buttonIndex);

/**
 @brief It replaces clicked delegate method with blocks
 */
-(id)initWithTitle:(NSString *)title
        completion:(ActionSheetCompletion)completion
 cancelButtonTitle:(NSString *)cancelButtonTitle
destructiveButtonTitle:(NSString *)destructiveButtonTitle
 otherButtonTitles:(NSString *)otherButtonTitles, ...;

@end
